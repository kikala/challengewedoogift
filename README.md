# Level 3
* The endpoint is localhost:8080/send, run the WebApp class to start it
* The endpoint is secured using spring security JWT.
* To call this endpoint we must have the 'wedooGiftUser' role
* Two users exists (wedoogift/password) and (unknown/password) , only user with username = wedoogift has wedooGiftUser role
#Test
* Connect to the application using localhost:8080/login with body {"username": "user","password":"password"} (eg {"wedoogift": "user","password":"password"}) and get the token returned
* call the endpoint /send with the token in the AUTHORIZATION header preceded by 'Bearer '

PS:
Au cas ou vous vous demendez pourquoi les heures des commits sont trop proches, j'avais le challenge
en gardant la meme structure que votre projet et après avoir push ma solution (https://gitlab.com/kikala/wedoogiftchalenge)
je me suis rendu compte qu'il etait un peu fastidieux pour le correcteur de s'y retrouver (j'avais meeme gardé le dossier frontend)
J'ai preferé reprendre le code sur ce projet pour plus de simplicité