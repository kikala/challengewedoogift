package org.wedoogift;

import org.junit.Before;
import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.Assert.assertEquals;

public class UserWalletTest {

    private Company wedooGift;
    private Company wedooFood;
    private User user;

    @Before
    public void setUp(){
        wedooGift = new Company(1,"wedooGift",100);
        wedooFood = new Company(1,"wedooFood",100);
        user = new User(1);
    }

    @Test
    public void gift_wallet_with_no_gift_card_has_only_his_initial_balance(){
        UserWallet giftWallet = new UserWallet(1,10);
        assertEquals(10,giftWallet.computeBalance());
    }

    @Test
    public void gift_wallet_with_all_gift_card_expired_has_only_his_initial_balance(){
        UserWallet giftWallet = new UserWallet(1,30);
        user.setGiftCardWallet(giftWallet);
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-02-01T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2021-03-01T00:00:00Z"), ZoneId.systemDefault());
        GiftCard giftCard1 = new GiftCard(wedooGift,user,50, startDate);
        GiftCard giftCard2 = new GiftCard(wedooGift,user,20, startDate);
        giftCard1.setClock(clock);
        giftCard2.setClock(clock);
        assertEquals(30,giftWallet.computeBalance());
    }

    @Test
    public void gift_wallet_with_some_gift_card_not_expired_has_his_initial_balance_plus_not_expired_gift_cards(){
        UserWallet giftWallet = new UserWallet(1,30);
        user.setGiftCardWallet(giftWallet);
        LocalDate startDate1 = LocalDate.now(Clock.fixed(Instant.parse("2020-02-01T00:00:00Z"), ZoneId.systemDefault()));
        GiftCard expiredGiftCard = new GiftCard(wedooGift,user,50, startDate1);
        LocalDate startDate2 = LocalDate.now(Clock.fixed(Instant.parse("2020-08-01T00:00:00Z"), ZoneId.systemDefault()));
        GiftCard notExpiredGiftCard = new GiftCard(wedooGift,user,10, startDate2);
        Clock clock = Clock.fixed(Instant.parse("2021-03-01T00:00:00Z"), ZoneId.systemDefault());
        expiredGiftCard.setClock(clock);
        notExpiredGiftCard.setClock(clock);
        assertEquals(40,giftWallet.computeBalance());
    }

    @Test
    public void gift_wallet_with_all_gift_card_not_expired_has_his_initial_balance_plus_all_gift_cards(){
        UserWallet giftWallet = new UserWallet(1,30);
        user.setGiftCardWallet(giftWallet);
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-02-01T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2020-03-01T00:00:00Z"), ZoneId.systemDefault());
        GiftCard giftCard1 = new GiftCard(wedooGift,user,50, startDate);
        GiftCard giftCard2 = new GiftCard(wedooGift,user,20, startDate);
        giftCard1.setClock(clock);
        giftCard2.setClock(clock);
        assertEquals(100,giftWallet.computeBalance());
    }

    @Test
    public void meal_wallet_with_no_gift_card_has_only_his_initial_balance(){
        UserWallet mealWallet = new UserWallet(2,10);
        assertEquals(10,mealWallet.computeBalance());
    }

    @Test
    public void meal_wallet_with_all_gift_card_expired_has_only_his_initial_balance(){
        UserWallet mealWallet = new UserWallet(1,30);
        user.setMealVoucherWallet(mealWallet);
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-04-01T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2021-03-01T00:00:00Z"), ZoneId.systemDefault());
        MealVoucher mealVoucher1 = new MealVoucher(wedooFood,user,50, startDate);
        MealVoucher mealVoucher2 = new MealVoucher(wedooFood,user,20, startDate);
        mealVoucher1.setClock(clock);
        mealVoucher2.setClock(clock);
        assertEquals(30,mealWallet.computeBalance());
    }

    @Test
    public void meal_wallet_with_some_gift_card_not_expired_has_his_initial_balance_plus_not_expired_gift_cards(){
        UserWallet mealWallet = new UserWallet(1,30);
        user.setMealVoucherWallet(mealWallet);
        LocalDate startDate1 = LocalDate.now(Clock.fixed(Instant.parse("2020-10-01T00:00:00Z"), ZoneId.systemDefault()));
        MealVoucher expiredMealVoucher = new MealVoucher(wedooFood,user,50, startDate1);
        LocalDate startDate2 = LocalDate.now(Clock.fixed(Instant.parse("2021-01-01T00:00:00Z"), ZoneId.systemDefault()));
        MealVoucher notExpiredMealVoucher = new MealVoucher(wedooFood,user,10, startDate2);
        Clock clock = Clock.fixed(Instant.parse("2021-03-01T00:00:00Z"), ZoneId.systemDefault());
        expiredMealVoucher.setClock(clock);
        notExpiredMealVoucher.setClock(clock);
        assertEquals(40,mealWallet.computeBalance());
    }

    @Test
    public void meal_wallet_with_all_gift_card_not_expired_has_his_initial_balance_plus_all_gift_cards(){
        UserWallet mealWallet = new UserWallet(1,30);
        user.setMealVoucherWallet(mealWallet);
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-04-01T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2021-02-01T00:00:00Z"), ZoneId.systemDefault());
        MealVoucher mealVoucher1 = new MealVoucher(wedooFood,user,50, startDate);
        MealVoucher mealVoucher2 = new MealVoucher(wedooFood,user,20, startDate);
        mealVoucher1.setClock(clock);
        mealVoucher2.setClock(clock);
        assertEquals(100,mealWallet.computeBalance());
    }
}