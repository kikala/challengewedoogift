package org.wedoogift;

import org.junit.Before;
import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GiftCardTest {

    private Company company;

    private User user;

    @Before
    public void setUp(){
        company = new Company(1,"wedoogift",100);
        user = new User(1);
    }

    @Test
    public void gift_card_created_today_is_not_expired(){
        GiftCard giftCard = new GiftCard(company,user,50, LocalDate.now());
        assertTrue(giftCard.isNotExpired());
    }

    @Test
    public void gift_card_created_2020_09_16_is_not_expired_at_2021_01_01(){
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-09-16T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2021-01-01T00:00:00Z"), ZoneId.systemDefault());
        GiftCard giftCard = new GiftCard(company,user,50, startDate);
        giftCard.setClock(clock);
        assertTrue(giftCard.isNotExpired());
    }

    @Test
    public void gift_card_created_2020_09_16_is_not_expired_at_2021_09_15(){
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-09-16T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2021-09-15T00:00:00Z"), ZoneId.systemDefault());
        GiftCard giftCard = new GiftCard(company,user,50, startDate);
        giftCard.setClock(clock);
        assertTrue(giftCard.isNotExpired());
    }

    @Test
    public void gift_card_created_2020_09_16_is_expired_at_2021_09_16(){
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-09-16T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2021-09-16T00:00:00Z"), ZoneId.systemDefault());
        GiftCard giftCard = new GiftCard(company,user,50, startDate);
        giftCard.setClock(clock);
        assertFalse(giftCard.isNotExpired());
    }

}