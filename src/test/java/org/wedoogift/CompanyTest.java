package org.wedoogift;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class CompanyTest {

    @Test(expected = InsufficientFundException.class)
    public void can_not_send_gift_card_to_user_when_amount_greater_than_balance(){
        Company company = new Company(1,"wedoogift",100);
        User user = new User(1);
        company.distributeGiftCard(user,150,LocalDate.now());
    }

    @Test(expected = InsufficientFundException.class)
    public void can_not_send_meal_voucher_to_user_when_amount_greater_than_balance(){
        Company company = new Company(1,"wedoofood",100);
        User user = new User(1);
        company.distributeMealVoucher(user,150,LocalDate.now());
    }

    @Test
    public void gift_card_is_sent_to_user_when_balance_is_sufficient(){
        Company company = new Company(1,"wedoogift",100);
        User user = new User(1);
        GiftCard giftCard = company.distributeGiftCard(user,30,LocalDate.now());
        assertNotNull(giftCard);
        assertEquals(70,company.getBalance());
        assertEquals(30,giftCard.getAmount());
        assertEquals(company,giftCard.getCompany());
        assertEquals(user,giftCard.getUser());
        assertTrue(user.hasGiftCard(giftCard));
    }

    @Test
    public void meal_voucher_is_sent_to_user_when_balance_is_sufficient(){
        Company company = new Company(1,"wedoogift",100);
        User user = new User(1);
        MealVoucher mealVoucher = company.distributeMealVoucher(user,30,LocalDate.now());
        assertNotNull(mealVoucher);
        assertEquals(70,company.getBalance());
        assertEquals(30,mealVoucher.getAmount());
        assertEquals(company,mealVoucher.getCompany());
        assertEquals(user,mealVoucher.getUser());
        assertTrue(user.hasMealVoucher(mealVoucher));
    }

}