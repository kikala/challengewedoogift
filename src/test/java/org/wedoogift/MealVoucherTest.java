package org.wedoogift;

import org.junit.Before;
import org.junit.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MealVoucherTest {

    private Company company;

    private User user;

    @Before
    public void setUp(){
        company = new Company(1,"wedoofood",100);
        user = new User(1);
    }

    @Test
    public void meal_voucher_created_2020_01_16_is_not_expired_at_2020_03_01(){
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-01-16T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2020-03-01T00:00:00Z"), ZoneId.systemDefault());
        MealVoucher mealVoucher = new MealVoucher(company,user,50, startDate);
        mealVoucher.setClock(clock);
        assertTrue(mealVoucher.isNotExpired());
    }

    @Test
    public void gift_card_created_2020_01_16_is_not_expired_at_2021_02_28(){
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-01-16T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2021-02-28T00:00:00Z"), ZoneId.systemDefault());
        MealVoucher mealVoucher = new MealVoucher(company,user,50, startDate);
        mealVoucher.setClock(clock);
        assertTrue(mealVoucher.isNotExpired());
    }

    @Test
    public void gift_card_created_2020_01_16_is_expired_at_2021_03_1(){
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-01-16T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2021-03-01T00:00:00Z"), ZoneId.systemDefault());
        MealVoucher mealVoucher = new MealVoucher(company,user,50, startDate);
        mealVoucher.setClock(clock);
        assertFalse(mealVoucher.isNotExpired());
    }

    @Test
    public void gift_card_created_2020_12_31_is_expired_at_2021_03_1(){
        LocalDate startDate = LocalDate.now(Clock.fixed(Instant.parse("2020-12-31T00:00:00Z"), ZoneId.systemDefault()));
        Clock clock = Clock.fixed(Instant.parse("2021-03-01T00:00:00Z"), ZoneId.systemDefault());
        MealVoucher mealVoucher = new MealVoucher(company,user,50, startDate);
        mealVoucher.setClock(clock);
        assertFalse(mealVoucher.isNotExpired());
    }
}