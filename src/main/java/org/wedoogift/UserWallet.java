package org.wedoogift;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class UserWallet {

    private int id;

    private long initialBalance;

    private List<Endowment> endowments = new ArrayList<>();

    public UserWallet() {
    }

    public UserWallet(int id, long initialBalance) {
        this.id = id;
        this.initialBalance = initialBalance;
    }

    public void addEndowment(Endowment endowment){
        this.endowments.add(endowment);
    }

    @JsonProperty("amount")
    public long computeBalance(){
        return this.endowments
                .stream()
                .filter(Endowment::isNotExpired)
                .mapToLong(Endowment::getAmount)
                .reduce(0,Long::sum) + this.initialBalance;
    }

    @JsonProperty("amount")
    public void setInitialBalance(long initialBalance) {
        this.initialBalance = initialBalance;
    }

    public int getId() {
        return id;
    }

    @JsonProperty("wallet_id")
    public void setId(int id) {
        this.id = id;
    }

    public boolean hasEndowment(Endowment endowment){
        return this.endowments.contains(endowment);
    }
}
