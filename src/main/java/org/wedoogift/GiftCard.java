package org.wedoogift;

import java.time.LocalDate;

public class GiftCard extends Endowment{

    private static int VALIDITY_DAYS = 365;

    public GiftCard(Company company, User user, long amount, LocalDate startDate) {
        super(company, user, amount, startDate);
        user.addGiftCard(this);
    }

    @Override
    protected LocalDate getExpiryDate() {
        return startDate.plusDays(VALIDITY_DAYS);
    }

    @Override
    public int getWalletId() {
        return WalletType.GIFT.getId();
    }

    @Override
    public String getEndDateAsString() {
        return startDate.plusDays(VALIDITY_DAYS - 1).toString();
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}
