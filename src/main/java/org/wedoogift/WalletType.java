package org.wedoogift;

public enum WalletType {

    GIFT(1),
    FOOD(2);

    private int id;

    WalletType(int id){this.id = id;}

    public int getId() {
        return id;
    }
}
