package org.wedoogift.web;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.wedoogift.WedooGiftPlatform;

import java.time.LocalDate;

@RestController
public class Controller {

    @PostMapping("/send")
    @PreAuthorize("hasAuthority('wedooGiftUser')")
    public WedooGiftPlatform distribute(@RequestBody WedooGiftPlatform platform){
        platform.sendGiftCard(1,1,50, LocalDate.of(2020,9,16));
        platform.sendGiftCard(1,2,100, LocalDate.of(2020,8,1));
        platform.sendGiftCard(2,3,1000, LocalDate.of(2020,5,1));
        //j'ai modifié la date de debut a 2021 pour que la carte reste valide
        platform.sendMealVoucher(1,1,250,LocalDate.of(2021,5,1));
        return platform;
    }

}
