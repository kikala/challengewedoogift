package org.wedoogift;

import java.time.LocalDate;

public class MealVoucher extends Endowment{

    public MealVoucher(Company company, User user, long amount, LocalDate startDate) {
        super(company, user, amount, startDate);
        user.addMealVoucher(this);
    }

    @Override
    public LocalDate getExpiryDate() {
        int expiryYear = this.startDate.getYear() + 1;
        return LocalDate.of(expiryYear,3,1);
    }

    @Override
    public int getWalletId() {
        return WalletType.FOOD.getId();
    }


}
